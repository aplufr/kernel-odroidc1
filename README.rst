Latest Linux kernel for ODROID-C1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a Linux Kernel for the ODROID C1 board, built from latest stable kernel availble from kernel.org.

The french related article is available here: https://www.aplu.fr/v2/post/2019/04/03/un-kernel-tout-neuf-pour-lodroid-c1.

See build-instruction.rst regarding compilation question.

How to use it?
++++++++++++++

All of the following instruction must be done as root.

1. clone this git repository to your card

::

     cd /root
     git clone --depth=1 https://gitlab.com/mulx/kernel-odroidc1 
     cd kernel-odroidc1
     VERSION=$(cat kernel.release)

2. Copy files to /boot (or /media/boot) and /lib/modules

::

   cp -a boot/uImage /boot/uImage-$VERSION
   cp -a boot/meson8b-odroidc1.dtb /boot/.
   cp -a lib/modules/$VERSION /lib/modules


3. Generate initramfs

::

   update-initramfs -c -k $VERSION
   mkimage -A arm -O linux -T ramdisk -C none -a 0 -e 0 -n uInitrd -d /boot/initrd.img-$VERSION /boot/uInitrd-$VERSION

4. Update the boot.ini

::

  cp boot/boot.ini /boot/boot.ini
  vim/nano /boot/boot.ini

Changes the following line with information corresponding to your card and update the uImage and uInitrd name to match the one you're using here.


::


  # Set MAC addr:
  setenv ethaddr "00:11:22:33:44:55"

  #set root UUID
  setenv rootdev "UUID=ab7ec76c-42f9-4eaa-ae98-45f666b3efed"

  fatload mmc 0:1 0x21000000 uImage-5.0.10
  fatload mmc 0:1 0x22000000 uInitrd-5.0.10
  #fatload mmc 0:1 0x21800000 meson8b_odroidc.dtb
  fatload mmc 0:1 0x21800000 meson8b-odroidc1.dtb

5. Force the network mac address and speed to avoid networking isssues

Networking issues are not alway present, this seem to depend where the ODROID is connected to (i.e. sometime, gigabit network is going to work fine).

::

   iface eth0 inet dhcp
     hwaddress ether 00:11:22:33:44:55
     up ethtool -s $IFACE speed 100 duplex full

6. Reboot and enjoy!
