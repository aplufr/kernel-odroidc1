How to build this kernel?
~~~~~~~~~~~~~~~~~~~~~~~~~~

This kernel is being cross-compiled from a Debian 9 AMD64 computer.

Install requirement 
+++++++++++++++++++

::

  apt install gcc-arm-linux-gnueabihf build-essential flex bison libssl-dev bc u-boot-tools ccache fakeroot ncurses-devel libncurses-dev lzop gcc-arm-none-eabi git


Exports variables
+++++++++++++++++

::

  export PATH=/usr/lib/ccache:$PATH
  export CROSS_COMPILE=arm-linux-gnueabihf-
  export LOADADDR=0x00208000
  export ARCH=arm
  export INSTALL_MOD_PATH=$HOME/compile/linux-modules/

Setup .config
+++++++++++++

::

  cp /THISGIT/config .config
  
Compile
+++++++

::

  make -j4 uImage dtbs modules

 
Install
+++++++

::

  make modules_install
  cp -a $INSTALL_MOD_PATH /YOURODROID/lib/modules/
  cp -a arch/arm/boot/uImage /YOURODROID/boot/uImage
  cp -a arch/arm/boot/dts/meson8b-odroidc1.dtb /YOURODROID/boot/.


